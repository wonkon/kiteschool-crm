<div id="top"></div>

# Kite School CRM

> Spring Boot Web App

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#general-information">General Info</a></li>
    <li><a href="#Built With">Built With</a></li>
    <li><a href="#heroku">Heroku</a></li>
    <li><a href="#room-for-improvement">Room for Improvement</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

### General Information

Simple customer relationship management application.
It enables the management of students and their instructors in the KiteSurfing school.
Work on increasing the functionality is ongoing.

### Built With

* [Java](https://www.oracle.com/pl/java/technologies/javase/jdk11-archive-downloads.html)
* [Spring Framework](https://spring.io/)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Spring Security](https://spring.io/projects/spring-security)
* [Maven](https://maven.apache.org/)
* [vaadin](https://vaadin.com/)
* [Heroku](https://www.heroku.com/platform)

### Heroku


[KiteSchoolCRM](https://alpha-kite-shool-crm.herokuapp.com/)

### Room for Improvement

To do:

- Payment for instructors by hours
- Statistics

### Acknowledgements

- This project was inspired by  [vaadin Flow](https://vaadin.com/docs/latest/flow/overview)

<p align="right">(<a href="#top">back to top</a>)</p>
