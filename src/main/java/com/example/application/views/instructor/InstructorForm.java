package com.example.application.views.instructor;

import com.example.application.data.entity.Instructor;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;

public class InstructorForm extends FormLayout {
    Binder<Instructor> binder = new BeanValidationBinder<>(Instructor.class);
    TextField firstName = new TextField("First name");
    TextField lastName = new TextField("Last name");
    TextField phone = new TextField("Phone");

    NumberField rate = new NumberField("rate");


    Button save = new Button("Save");
    Button delete = new Button("Delete");
    Button close = new Button("Close");
    private Instructor instructor;

    public InstructorForm() {
        addClassName("student-form");
        binder.bindInstanceFields(this);

        add(
                firstName,
                lastName,
                phone,
                rate,
                createButtonsLayout()
        );
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
        binder.readBean(instructor);

    }

    private HorizontalLayout createButtonsLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new InstructorForm.DeleteEvent(this, instructor)));
        close.addClickListener(event -> fireEvent(new InstructorForm.CloseEvent(this)));

        save.addClickShortcut(Key.ENTER);
        close.addClickShortcut(Key.ESCAPE);

        return new HorizontalLayout(save, delete, close);
    }

    private void validateAndSave() {
        try {
            binder.writeBean(instructor);
            fireEvent(new InstructorForm.SaveEvent(this, instructor));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    // Events
    public static abstract class InstructorFormFormEvent extends ComponentEvent<InstructorForm
            > {
        private Instructor instructor;

        protected InstructorFormFormEvent(InstructorForm source, Instructor instructor) {
            super(source, false);
            this.instructor = instructor;
        }

        public Instructor getInstructor() {
            return instructor;
        }
    }

    public static class SaveEvent extends InstructorForm.InstructorFormFormEvent {
        SaveEvent(InstructorForm source, Instructor instructor) {
            super(source, instructor);
        }
    }

    public static class DeleteEvent extends InstructorForm.InstructorFormFormEvent {
        DeleteEvent(InstructorForm source, Instructor instructor) {
            super(source, instructor);
        }

    }

    public static class CloseEvent extends InstructorForm.InstructorFormFormEvent {
        CloseEvent(InstructorForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(
            Class<T> eventType,
            ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
