package com.example.application.views.instructor;

import com.example.application.data.entity.Instructor;
import com.example.application.data.service.CrmService;
import com.example.application.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import javax.annotation.security.PermitAll;

@PageTitle("Instructor")
@Route(value = "instructor", layout = MainLayout.class)
@PermitAll
public class InstructorView extends VerticalLayout {
    Grid<Instructor> grid = new Grid<>(Instructor.class);
    TextField filterText = new TextField();
    InstructorForm form;
    private CrmService service;

    public InstructorView(CrmService service) {
        this.service = service;
        addClassName("instructor-view");
        setSizeFull();

        configureGrid();
        configureForm();

        add(getToolbar(), getContent());
        updateList();
        closeEditor();
    }

    private void closeEditor() {
        form.setInstructor(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    private void updateList() {
        grid.setItems(service.findAllInstructorBy(filterText.getValue()));
    }

    private Component getContent() {
        SplitLayout content = new SplitLayout(grid, form);
        content.setSplitterPosition(78);
        content.addClassName("content");
        content.setSizeFull();

        return content;
    }

    private void configureForm() {
        form = new com.example.application.views.instructor.InstructorForm();
        form.setWidth("20em");

        form.addListener(InstructorForm.SaveEvent.class, this::saveInstructor);
        form.addListener(InstructorForm.DeleteEvent.class, this::deleteInstructor);
        form.addListener(InstructorForm.CloseEvent.class, w -> closeEditor());
    }

    private void deleteInstructor(InstructorForm.DeleteEvent event) {
        service.deleteInstructor(event.getInstructor());
        updateList();
        closeEditor();

    }

    private void saveInstructor(InstructorForm.SaveEvent event) {
        service.saveInstructor(event.getInstructor());
        updateList();
        closeEditor();
    }

    private void configureGrid() {
        grid.addClassNames("contact-grid");
        grid.setSizeFull();
        grid.setColumns("firstName", "lastName","phone","rate");
        grid.addColumn(Instructor::getStudentCount).setHeader("StudentCount");
        grid.getColumns().forEach(col -> col.setAutoWidth(true));

        grid.asSingleSelect().addValueChangeListener(e -> editInstructor(e.getValue()));
    }

    private void editInstructor(Instructor instructor) {
        if (instructor == null) {
            closeEditor();
        } else {
            form.setInstructor(instructor);
            form.setVisible(true);
            addClassNames("editing");
        }
    }

    private HorizontalLayout getToolbar() {
        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addInstructorButton = new Button("Add instructor");
        addInstructorButton.addClickListener(e -> addInstructor());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addInstructorButton);
        toolbar.addClassName("toolbar");
        return toolbar;
    }

    private void addInstructor() {
        grid.asSingleSelect().clear();
        editInstructor(new Instructor());
    }
}
