package com.example.application.views.student;

import com.example.application.data.entity.Instructor;
import com.example.application.data.entity.Student;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;

import java.util.List;

public class StudentForm extends FormLayout {
    Binder<Student> binder = new BeanValidationBinder<>(Student.class);
    TextField firstName = new TextField("First name");
    TextField lastName = new TextField("Last name");
    TextField phone = new TextField("Phone");
    EmailField email = new EmailField("Email");
    ComboBox<Instructor> instructor = new ComboBox<>("Instructor");

    Checkbox available = new Checkbox("Active");

    Button save = new Button("Save");
    Button delete = new Button("Delete");
    Button close = new Button("Close");
    private Student student;

    public StudentForm(List<Instructor> instructors) {
        addClassName("student-form");
        binder.bindInstanceFields(this);

        instructor.setItems(instructors);
        instructor.setItemLabelGenerator(Instructor::getFirstName);
        add(
                firstName,
                lastName,
                email,
                phone,
                instructor,
                createButtonsLayout()
        );
    }

    public void setStudent(Student student) {
        this.student = student;
        binder.readBean(student);

    }

    private HorizontalLayout createButtonsLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new DeleteEvent(this, student)));
        close.addClickListener(event -> fireEvent(new CloseEvent(this)));

        save.addClickShortcut(Key.ENTER);
        close.addClickShortcut(Key.ESCAPE);

        available.getElement().getStyle().set("margin", "auto");

        return new HorizontalLayout(save, delete, close, available);
    }

    private void validateAndSave() {
        try {
            binder.writeBean(student);
            fireEvent(new SaveEvent(this, student));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public static abstract class StudentFormEvent extends ComponentEvent<StudentForm> {
        private final Student student;

        protected StudentFormEvent(StudentForm source, Student student) {
            super(source, false);
            this.student = student;
        }

        public Student getStudent() {
            return student;
        }
    }

    public static class SaveEvent extends StudentFormEvent {
        SaveEvent(StudentForm source, Student student) {
            super(source, student);
        }
    }

    public static class DeleteEvent extends StudentFormEvent {
        DeleteEvent(StudentForm source, Student student) {
            super(source, student);
        }

    }

    public static class CloseEvent extends StudentFormEvent {
        CloseEvent(StudentForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
