package com.example.application.views.student;

import com.example.application.data.entity.Student;
import com.example.application.data.service.CrmService;
import com.example.application.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import javax.annotation.security.PermitAll;

@PageTitle("Active Student")
@Route(value = "active", layout = MainLayout.class)
@PermitAll
public class ActiveStudentView extends VerticalLayout {
    Grid<Student> grid = new Grid<>(Student.class);
    ActiveStudentForm form;
    private final CrmService service;

    public ActiveStudentView(CrmService service) {
        this.service = service;
        addClassName("student-view");
        setSizeFull();

        configureGrid();
        configureForm();

        add(
                getContent()
        );
        onlyAvailableList();
    }

    private void onlyAvailableList() {
        grid.setItems(service.findStudentByAvailable());
    }

    private Component getContent() {
        SplitLayout content = new SplitLayout(grid, form);
        content.setOrientation(SplitLayout.Orientation.VERTICAL);
        content.setSplitterPosition(50);
        content.addClassName("content");
        content.setSizeFull();

        return content;
    }

    private void configureForm() {
        form = new ActiveStudentForm(service.findAllInstructors(), service.findAllSkills());
        form.setWidth("auto");
        form.addListener(ActiveStudentForm.SaveEvent.class, this::saveStudent);
        form.addListener(ActiveStudentForm.ClearEvent.class, this::clearForm);

    }

    private void clearForm(ActiveStudentForm.ClearEvent event) {
        form.setStudent(null);
    }

    private void saveStudent(ActiveStudentForm.SaveEvent event) {
        service.saveStudent(event.getStudent());
        onlyAvailableList();
    }

    private void configureGrid() {
        grid.addClassNames("student-grid");
        grid.setSizeFull();
        grid.setColumns("firstName", "lastName", "hours");
        grid.addColumn(student -> student.getSkills().getName()).setHeader("Skills");
        grid.addColumn(student -> student.getInstructor().getFirstName()).setHeader("Instructor Name");
        grid.getColumns().forEach(col -> col.setAutoWidth(true));
        grid.asSingleSelect().addValueChangeListener(e -> editStudent(e.getValue()));
    }

    private void editStudent(Student student) {
        form.setStudent(student);
        addClassNames("editing");
    }
}