package com.example.application.views.student;

import com.example.application.data.entity.Student;
import com.example.application.data.service.CrmService;
import com.example.application.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import javax.annotation.security.PermitAll;

@PageTitle("Students")
@Route(value = "student", layout = MainLayout.class)
@PermitAll
public class StudentView extends VerticalLayout {

    Grid<Student> grid = new Grid<>(Student.class);
    TextField filterText = new TextField();
    StudentForm form;
    private final CrmService service;

    public StudentView(CrmService service) {
        this.service = service;
        addClassName("student-view");
        setSizeFull();

        configureGrid();
        configureForm();

        add(
                getToolbar(),
                getContent()
        );
        updateList();
        closeEditor();
    }

    private void closeEditor() {
        form.setStudent(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    private void updateList() {
        grid.setItems(service.findAllStudentBy(filterText.getValue()));
    }

    private Component getContent() {
        SplitLayout content = new SplitLayout(grid, form);
        content.setSplitterPosition(70);
        content.addClassName("content");
        content.setSizeFull();

        return content;
    }

    private void configureForm() {
        form = new StudentForm(service.findAllInstructors());
        form.setWidth("25em");

        form.addListener(StudentForm.SaveEvent.class, this::saveStudent);
        form.addListener(StudentForm.DeleteEvent.class, this::deleteStudent);
        form.addListener(StudentForm.CloseEvent.class, w -> closeEditor());
    }

    private void deleteStudent(StudentForm.DeleteEvent event) {
        service.deleteStudent(event.getStudent());
        updateList();
        closeEditor();

    }

    private void saveStudent(StudentForm.SaveEvent event) {
        service.saveStudent(event.getStudent());
        updateList();
        closeEditor();
    }

    private void configureGrid() {
        grid.addClassNames("student-grid");
        grid.setSizeFull();
        grid.setColumns("firstName", "lastName", "email","phone");
        grid.addColumn(student -> student.getSkills().getName()).setHeader("Skills");
        grid.addColumn(student -> student.getInstructor().getFirstName()).setHeader("Instructor Name");

        LitRenderer<Student> importantRenderer = LitRenderer.<Student>of(
                        "<vaadin-icon icon='vaadin:${item.icon}' style='width: var(--lumo-icon-size-s); height: var(--lumo-icon-size-s); color: ${item.color};'></vaadin-icon>")
                .withProperty("icon", available -> available.isAvailable() ? "check" : "minus").withProperty("color",
                        available -> available.isAvailable()
                                ? "var(--lumo-primary-text-color)"
                                : "var(--lumo-disabled-text-color)");

        grid.addColumn(importantRenderer).setHeader("Active").setAutoWidth(true);

        grid.getColumns().forEach(col -> col.setAutoWidth(true));

        grid.asSingleSelect().addValueChangeListener(e -> editStudent(e.getValue()));
    }

    private void editStudent(Student student) {
        if (student == null) {
            closeEditor();
        } else {
            form.setStudent(student);
            form.setVisible(true);
            addClassNames("editing");
        }
    }

    private HorizontalLayout getToolbar() {
        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addStudentButton = new Button("Add student");
        addStudentButton.addClickListener(e -> addStudent());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addStudentButton);
        toolbar.addClassName("toolbar");
        return toolbar;
    }

    private void addStudent() {
        grid.asSingleSelect().clear();
        editStudent(new Student());
    }
}
