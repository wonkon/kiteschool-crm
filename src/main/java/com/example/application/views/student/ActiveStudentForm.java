package com.example.application.views.student;

import com.example.application.data.entity.Instructor;
import com.example.application.data.entity.Skills;
import com.example.application.data.entity.Student;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.shared.Registration;

import java.util.List;

public class ActiveStudentForm extends FormLayout {
    Binder<Student> binder = new BeanValidationBinder<>(Student.class);
    ComboBox<Skills> skills = new ComboBox<>("Skills");
    ComboBox<Instructor> instructor = new ComboBox<>("Instructor");
    NumberField hours = new NumberField("Number of hours done");
    Checkbox available = new Checkbox("Active");
    TextArea noteArea = new TextArea("Note");
    Button save = new Button("Save");
    Button clear = new Button("Clear");
    private Student student;

    public ActiveStudentForm(List<Instructor> instructors, List<Skills> skillsList) {
        addClassName("student-form");
        binder.bindInstanceFields(this);

        hours.setStep(0.5);
        hours.setHasControls(true);

        noteArea.setWidthFull();
        int charLimit = 100;
        noteArea.setMaxLength(charLimit);
        noteArea.setValueChangeMode(ValueChangeMode.EAGER);
        noteArea.addValueChangeListener(e -> {
            e.getSource().setHelperText(e.getValue().length() + "/" + charLimit);
        });


        skills.setItems(skillsList);
        skills.setItemLabelGenerator(Skills::getName);

        instructor.setItems(instructors);
        instructor.setItemLabelGenerator(Instructor::getFirstName);

        add(
                instructor,
                skills,
                hours,
                noteArea,
                createButtonsLayout()
        );
        setResponsiveSteps(
                // Use one column by default
                new ResponsiveStep("0", 1),
                // Use two columns, if the layout's width exceeds 320px
                new ResponsiveStep("320px", 2),
                // Use three columns, if the layout's width exceeds 500px
                new ResponsiveStep("500px", 3)
        );
    }
    private HorizontalLayout createButtonsLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        clear.addThemeVariants(ButtonVariant.LUMO_ERROR);

        save.addClickListener(event -> validateAndSave());
        clear.addClickListener(event -> fireEvent(new ActiveStudentForm.ClearEvent(this)));

        save.addClickShortcut(Key.ENTER);
        clear.addClickShortcut(Key.ESCAPE);

        available.getElement().getStyle().set("margin", "auto");

        return new HorizontalLayout(save, clear, available);
    }
    public void setStudent(Student student) {
        this.student = student;
        binder.readBean(student);

    }
    private void validateAndSave() {
        try {
            binder.writeBean(student);
            fireEvent(new ActiveStudentForm.SaveEvent(this, student));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }


    public static abstract class StudentFormEvent extends ComponentEvent<ActiveStudentForm> {
        private final Student student;

        protected StudentFormEvent(ActiveStudentForm source, Student student) {
            super(source, false);
            this.student = student;
        }

        public Student getStudent() {
            return student;
        }
    }

    public static class SaveEvent extends ActiveStudentForm.StudentFormEvent {
        SaveEvent(ActiveStudentForm source, Student student) {
            super(source, student);
        }
    }

    public static class ClearEvent extends ActiveStudentForm.StudentFormEvent {
        ClearEvent(ActiveStudentForm source) {

            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
