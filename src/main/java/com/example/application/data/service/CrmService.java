package com.example.application.data.service;


import com.example.application.data.entity.Instructor;
import com.example.application.data.entity.Skills;
import com.example.application.data.entity.Student;
import com.example.application.data.repository.InstructorRepo;
import com.example.application.data.repository.SkillsRepo;
import com.example.application.data.repository.StudentRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CrmService {

    private final StudentRepo studentRepo;
    private final InstructorRepo instructorRepo;
    private final SkillsRepo skillsRepo;

    public CrmService(StudentRepo studentRepo,
                      InstructorRepo instructorRepo,
                      SkillsRepo skillsRepo) {
        this.studentRepo = studentRepo;
        this.instructorRepo = instructorRepo;
        this.skillsRepo = skillsRepo;
    }

    public List<Student> findStudentByAvailable() {
        return studentRepo.findAll().stream()
                .filter(Student::isAvailable)
                .collect(Collectors.toList());
    }

    public List<Student> findAllStudentBy(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return studentRepo.findAll();
        } else {
            return studentRepo.search(stringFilter);
        }
    }

    public List<Instructor> findAllInstructorBy(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return instructorRepo.findAll();
        } else {
            return instructorRepo.search(stringFilter);
        }
    }

    public long countStudent() {
        return studentRepo.count();
    }

    public long instructorStudent() {
        return instructorRepo.count();
    }

    public void deleteStudent(Student student) {
        studentRepo.delete(student);
    }

    public void deleteInstructor(Instructor instructor) {
        instructorRepo.delete(instructor);
    }

    public void saveStudent(Student student) {
        if (student == null) {
            System.err.println("Student is null.");
            return;
        }
        studentRepo.save(student);
    }

    public void saveInstructor(Instructor instructor) {
        if (instructor == null) {
            System.err.println("Student is null.");
            return;
        }
        instructorRepo.save(instructor);
    }

    public List<Student> findAllStudent() {
        return studentRepo.findAll();
    }

    public List<Instructor> findAllInstructors() {
        return instructorRepo.findAll();
    }

    public List<Skills> findAllSkills() {
        return skillsRepo.findAll();
    }
}
