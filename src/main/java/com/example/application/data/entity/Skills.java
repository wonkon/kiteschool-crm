package com.example.application.data.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Data
@NoArgsConstructor
@Entity
public class Skills extends AbstractEntity {
    private String name;

    public Skills(String name) {
        this.name = name;
    }

}
