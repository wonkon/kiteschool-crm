package com.example.application.data.entity;

import lombok.Data;
import org.hibernate.annotations.Formula;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
public class Instructor extends AbstractEntity {

    @NotEmpty
    private String firstName = "";
    @NotEmpty
    private String lastName = "";
    @NotEmpty
    private String phone = "";

    private double rate;


    @OneToMany(mappedBy = "instructor")
    private List<Student> students = new LinkedList<>();

    @Formula("(select count(c.id) from Student c where c.instructor_id = id)")
    private int studentCount;


}
