package com.example.application.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class Student extends AbstractEntity {

    @NotEmpty
    private String firstName = "";
    @NotEmpty
    private String lastName = "";
    @NotEmpty
    private String phone = "";

    private String noteArea;

    private boolean available;

    private double hours;

    @ManyToOne
    @JoinColumn(name = "instructor_id")
    @NotNull
    @JsonIgnoreProperties({"students"})
    private Instructor instructor;

    @NotNull
    @ManyToOne
    private Skills skills;

    @Email
    @NotEmpty
    private String email = "";
}
