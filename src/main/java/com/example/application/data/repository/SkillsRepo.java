package com.example.application.data.repository;

import com.example.application.data.entity.Skills;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillsRepo extends JpaRepository<Skills, Integer> {
}
