package com.example.application.data.repository;

import com.example.application.data.entity.Instructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InstructorRepo extends JpaRepository<Instructor, Integer> {
    @Query("select i from Instructor i " +
            "where lower(i.firstName) like lower(concat('%', :searchTerm, '%')) " +
            "or lower(i.lastName) like lower(concat('%', :searchTerm, '%'))")
    List<Instructor> search(@Param("searchTerm") String searchTerm);
}
