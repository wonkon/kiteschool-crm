package com.example.application.data.generator;

import com.example.application.data.entity.Instructor;
import com.example.application.data.entity.Skills;
import com.example.application.data.entity.Student;
import com.example.application.data.repository.InstructorRepo;
import com.example.application.data.repository.SkillsRepo;
import com.example.application.data.repository.StudentRepo;
import com.vaadin.exampledata.DataType;
import com.vaadin.exampledata.ExampleDataGenerator;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringComponent
public class DataGenerator {

    @Bean
    public CommandLineRunner loadData(StudentRepo studentRepo, InstructorRepo instructorRepo,
                                      SkillsRepo skillsRepo) {

        return args -> {
            Logger logger = LoggerFactory.getLogger(getClass());
            if (studentRepo.count() != 0L) {
                logger.info("Using existing database");
                return;
            }
            int seed = 852;
            logger.info("... generating 10 Instructor entities...");
            ExampleDataGenerator<Instructor> instructorGenerator = new ExampleDataGenerator<>(Instructor.class,
                    LocalDateTime.now());
            instructorGenerator.setData(Instructor::setFirstName, DataType.FIRST_NAME);
            instructorGenerator.setData(Instructor::setLastName, DataType.LAST_NAME);
            instructorGenerator.setData(Instructor::setPhone,DataType.PHONE_NUMBER);
            instructorGenerator.setData(Instructor::setRate,DataType.PRICE);

            List<Instructor> instructors = instructorRepo.saveAll(instructorGenerator.create(10, seed));

            List<Skills> skills = skillsRepo
                    .saveAll(Stream.of("lvl-1-Discovery", "lvl-2-Intermidiate", "lvl-3-Independent", "lvl-4-Advance")
                            .map(Skills::new).collect(Collectors.toList()));

            logger.info("... generating 50 Student entities...");
            ExampleDataGenerator<Student> studentGenerator = new ExampleDataGenerator<>(Student.class,
                    LocalDateTime.now());
            studentGenerator.setData(Student::setFirstName, DataType.FIRST_NAME);
            studentGenerator.setData(Student::setLastName, DataType.LAST_NAME);
            studentGenerator.setData(Student::setEmail, DataType.EMAIL);
            studentGenerator.setData(Student::setAvailable, DataType.BOOLEAN_50_50);
            studentGenerator.setData(Student::setPhone, DataType.PHONE_NUMBER);
            studentGenerator.setData(Student::setNoteArea, DataType.WORD);
            studentGenerator.setData(Student::setHours,DataType.PRICE);

            Random r = new Random(seed);
            List<Student> students = studentGenerator.create(50, seed).stream().peek(student -> {
                student.setInstructor(instructors.get(r.nextInt(instructors.size())));
                student.setSkills(skills.get(r.nextInt(skills.size())));
            }).collect(Collectors.toList());

            studentRepo.saveAll(students);

            logger.info("Generated demo data");
        };
    }
}